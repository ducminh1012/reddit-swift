# Pre-work - Reddit-Swift

Reddit-Swift is a reddit demo application for iOS.

## User Stories

The following functionality is complete:

* - User can add new topics.
* - Upvote and downvote multiple times.
* - Topic has maximum 255 characters.
* - List of top 20 topics order by vote count.

## Video Walkthrough

Here's a walkthrough of implemented user stories:

<img src='https://postimg.org/image/y6c2gj8sl/' title='Video Walkthrough' width='' alt='Video Walkthrough' />

GIF created with [LiceCap](http://www.cockos.com/licecap/).

