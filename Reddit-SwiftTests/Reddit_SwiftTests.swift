//
//  Reddit_SwiftTests.swift
//  Reddit-SwiftTests
//
//  Created by Kyou on 192//18.
//  Copyright © 2018 Duc Minh. All rights reserved.
//

import XCTest
@testable import Reddit_Swift

class Reddit_SwiftTests: XCTestCase {
    
    var viewController: ViewController!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        viewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        viewController.loadViewIfNeeded()
        
        // Add some test topics
        viewController.addTopic(text: "Test topic 1")
        viewController.addTopic(text: "Test topic 2")
        viewController.addTopic(text: "Test topic 3")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testUpVote() {
        let index = IndexPath(row: 0, section: 0)
        viewController.upVote(index: index)
        
        let topic = viewController.topics[index.row]
        XCTAssertEqual(topic.voteCount, 1) // Test pass if voteCount increase to 1
    }
    
    func testDownVote() {
        let index = IndexPath(row: 0, section: 0)
        
        viewController.upVote(index: index)
        let topic1 = viewController.topics[index.row]
        XCTAssertEqual(topic1.voteCount, 1) // Increase voteCount to 1 to test down vote
        viewController.downVote(index: index)
        let topic2 = viewController.topics[index.row]
        XCTAssertEqual(topic2.voteCount, 0) // Test pass if voteCount decrease to 0
    }
    
    func testAddTopic() {
        viewController.addTopic(text: "Test topic 4")
        XCTAssert(viewController.topics.count == 4) // Test pass if number of topics = 4
    }
}
