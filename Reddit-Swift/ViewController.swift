//
//  ViewController.swift
//  Reddit-Swift
//
//  Created by Kyou on 192//18.
//  Copyright © 2018 Duc Minh. All rights reserved.
//

import UIKit

// Global Constants
let kMaxlength = 255

class ViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Actions
    @IBAction func onAddToppicButton(_ sender: UIBarButtonItem) {
        addTopicPopup = UIAlertController(title: "New post (\(kMaxlength))", message: "", preferredStyle: .alert)
        addTopicPopup.addTextField(configurationHandler: {(_ textField: UITextField) -> Void in
            textField.placeholder = "New post"
            textField.delegate = self
        })
        
        let confirmAction = UIAlertAction(title: "Add", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            guard let text = self.addTopicPopup.textFields?[0].text, text != "" else { return }
            self.addTopic(text: text)
        })
        
        addTopicPopup.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
            print("Canelled")
        })
        
        addTopicPopup.addAction(cancelAction)
        present(addTopicPopup, animated: true, completion: nil)
    }
    
    // MARK: Properties
    var topics = [Topic]() {
        didSet {
            topics.sort{ (t1, t2) -> Bool in
                return t1.voteCount > t2.voteCount
            }
            self.tableView.reloadData()
        }
    }
    
    var addTopicPopup = UIAlertController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func addTopic(text: String) {
        let topic = Topic(title: text, voteCount: 0)
        self.topics.append(topic)
        self.tableView.reloadData()
    }
    
    func upVote(index: IndexPath) {
        var topic = topics[index.row]
        topic.voteCount += 1
        topics[index.row] = topic
    }
    
    func downVote(index: IndexPath) {
        var topic = topics[index.row]
        topic.voteCount -= 1
        topics[index.row] = topic
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if topics.count >= 20 {
            return 20
        } else {
            return topics.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ToppicCell.identifier) as? ToppicCell else { return UITableViewCell() }
        let topic = topics[indexPath.row]
        cell.topic = topic
        cell.delegate = self
        return cell
    }
}

// MARK: UITextFieldDelegate
extension ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        if (newLength <= kMaxlength) {
            addTopicPopup.title = "New Post (\(kMaxlength - newLength))"
            return true
        } else {
            return false
        }
    }
}

// MARK: TopicCellDelegate
extension ViewController: TopicCellDelegate {
    func topicCell(cell: ToppicCell, didTapUpVote button: UIButton) {
        guard let index = tableView.indexPath(for: cell) else { return }
        upVote(index: index)
    }
    
    func topicCell(cell: ToppicCell, didTapDownVote button: UIButton) {
        guard let index = tableView.indexPath(for: cell) else { return }
        downVote(index: index)
    }
}
