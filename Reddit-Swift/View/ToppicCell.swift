//
//  ToppicCell.swift
//  Reddit-Swift
//
//  Created by Kyou on 192//18.
//  Copyright © 2018 Duc Minh. All rights reserved.
//

import UIKit

protocol TopicCellDelegate: class {
    func topicCell(cell: ToppicCell, didTapUpVote button: UIButton)
    func topicCell(cell: ToppicCell, didTapDownVote button: UIButton)
}

class ToppicCell: UITableViewCell {
    // MARK: Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var voteCountLabel: UILabel!
    
    // MARK: Actions
    @IBAction func onUpVoteButton(_ sender: UIButton) {
        delegate?.topicCell(cell: self, didTapUpVote: sender)
    }
    
    @IBAction func onDownVoteButton(_ sender: UIButton) {
        guard let voteCount = topic?.voteCount, voteCount > 0 else { return }
        delegate?.topicCell(cell: self, didTapDownVote: sender)
    }
    
    // MARK: Properties
    static let identifier = "ToppicCell"
    
    weak var delegate: TopicCellDelegate?
    
    var topic: Topic? {
        didSet {
            titleLabel.text = topic?.title
            voteCountLabel.text = topic?.voteCount.description
        }
    }
}
